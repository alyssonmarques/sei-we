class ProcessosController {

    constructor() {
        this._processos = [];
    }

    adicionarProcesso(processo) {
        // console.log(processo);
        this._processos.push(processo);
    }

    obterCSVProcessos() {
        var sep = '\t';
        var csv = 'Cod Processo' + sep 
        + 'Tipo Processo' + sep 
        + 'Usuário' + sep 
        + 'Usuário (CPF)' + sep 
        + 'Anotação' + sep
        + 'Anotação (CPF)' + sep
        + 'Marcador' + sep
        + 'Marcador (Texto)' + sep
        + 'Ret. Prog.' + sep
        + 'Ret. Prog. (Demandante)' + sep
        + '\r\n' ;
        this._processos.forEach((processo, index) => {
            csv += processo._codProcesso + sep 
            + processo._tipo + sep 
            + processo._usuario._nome + sep 
            + processo._usuario._cpf  + sep
            + (processo._anotacao != null ? processo._anotacao : '')  + sep
            + (processo._anotacaoCPF != null ? processo._anotacaoCPF : '') + sep
            + (processo._marcador != null ? processo._marcador : '')  + sep
            + (processo._marcadorTexto != null ? processo._marcadorTexto : '')  + sep
            + (processo._dataRetornoProgramado != null ? processo._dataRetornoProgramado : '')  + sep
            + (processo._areaRetornoProgramado != null ? processo._areaRetornoProgramado : '')  + sep
            + '\r\n';
        });
        return csv;
    }

}