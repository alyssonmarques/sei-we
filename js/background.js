var processosController = new ProcessosController();
var extraindo = false;

var dadosExtracaoEstatisticas = {
    extraindo: false,
    dataInicio: null,
    dataFim: null
}

function adicionarProcesso(processo) {
    console.log(processo);
}

browser.runtime.onMessage.addListener((request, sender, sendResponse) => {
    if(request.message === 'extraindo') {
        console.log('background.js tratando mensagem extraindo');
        sendResponse({data: extraindo});
    }
    if(request.message === 'iniciarExtracao') {
        console.log('background.js tratando mensagem iniciarExtracao');
        extraindo = true;
        sendResponse({data: extraindo});
    }
    if(request.message === 'downloadProcessos') {
        console.log('background.js tratando mensagem downloadProcessos');
        sendResponse({data: processosController.obterCSVProcessos()});
    }
    if(request.message === 'adicionarProcesso') {
        console.log('background.js tratando mensagem adicionarProcesso');
        processosController.adicionarProcesso(request.processo);
        // adicionarProcesso(request.processo);
    }

    // Estatisticas ============
    if(request.message === 'getDadosExtracaoEstatistica') {
        console.log('background.js tratando mensagem getDadosExtracaoEstatistica');
        sendResponse({dadosExtracaoEstatisticas});
    }
    if(request.message === 'setDadosExtracaoEstatistica') {
        console.log('background.js tratando mensagem setDadosExtracaoEstatistica');
        dadosExtracaoEstatisticas = request.data;
        sendResponse({dadosExtracaoEstatisticas});
    }
});
