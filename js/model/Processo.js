class Processo {

    constructor(codProcesso, aElement) {
        this._codProcesso = codProcesso;
        this._aElement = aElement;
        this._usuario = new Usuario('', '');
        this._tipo = 'Não definido';
        this._anotacao = null;
        this._anotacaoCPF = null;
        this._marcador = null;
        this._marcadorTexto = null;
        this._areaRetornoProgramado = null;
        this._dataRetornoProgramado = null;
    }

    get processo() {
        return this._codProcesso;
    }

    get aElement() {
        return this._aElement;
    }

    get usuario() {
        return this._usuario;
    }

    get tipo() {
        return this._tipo;
    }

    get anotacao() {
        return this._anotacao;
    }

    get anotacaoCPF() {
        return this._anotacaoCPF;
    }

    get marcador() {
        return this._marcador;
    }

    get marcadorTexto() {
        return this._marcadorTexto;
    }

    get areaRetornoProgramado() {
        return this._areaRetornoProgramado;
    }

    get dataRetornoProgramado() {
        return this._dataRetornoProgramado;
    }

    setCodProcesso(nup) {
        this._codProcesso = nup;
    }

    setAElement(a) {
        this._aElement = a;
    }

    setUsuario(usuario) {
        this._usuario = usuario;
    }

    setTipo(tipo) {
        this._tipo = tipo;
    }

    setAnotacao(anotacao) {
        this._anotacao = anotacao;
    }

    setAnotacaoCPF(anotacaoCPF) {
        this._anotacaoCPF = anotacaoCPF;
    }

    setMarcador(marcador) {
        this._marcador = marcador;
    }

    setMarcadorTexto(marcadorTexto) {
        this._marcadorTexto = marcadorTexto;
    }

    setAreaRetornoProgramado(areaRP) {
        this._areaRetornoProgramado = areaRP;
    }

    setDataRetornoProgramado(dataRP) {
        this._dataRetornoProgramado = dataRP;
    }

    print() {
        console.log('\t ' + this._codProcesso);
    }
}