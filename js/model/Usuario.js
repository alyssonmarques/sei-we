class Usuario {

    constructor(nome, cpf) {
        this._nome = nome;
        this._cpf = cpf;
    }

    get nome() {
        return this._nome;
    }

    get cpf() {
        return this._cpf;
    }

    print() {
        console.log('Nome: ' + this._nome + '\t CPF: ' + this._cpf);
    }

}