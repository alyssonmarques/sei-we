class ExtratorEstatisticas {

    constructor() {
        this._estatisticas = {};
        this._dadosExtracao = {
            extraindo: false
        };
    }

    execute() {
        console.log('Início da leitura leitura das estatísticas');
        this.obterDadosExtracao();
        this.montarFormularioExtracao();
        // window.alert(this._dadosExtracao.extraindo);
        if(this._dadosExtracao.extraindo) {
            this.configurarFormularioEmExtracao();
        }
    }

    configurarFormularioEmExtracao() {
        let dateInicio = document.querySelector('input#dateInicio');
        dateInicio.disabled = true;

        let dateFim = document.querySelector('input#dateFim');
        dateFim.disabled = true;

        let botaoExtrair = document.querySelector('button#botaoExtrair');
        botaoExtrair.disabled = true;

        let botaoCancelar = document.createElement('button');
        botaoCancelar.setAttribute('id', 'botaoCancelar');
        botaoCancelar.innerText = 'Cancelar';
        botaoCancelar.addEventListener('click', this.botaoCancelarOnClickListener);

        let divBotoes = document.querySelector('div#divBotoes');
        divBotoes.appendChild(botaoCancelar);
    }

    obterDadosExtracao() {
        let retorno = browser.runtime.sendMessage({
            'message': 'getDadosExtracaoEstatistica'
        });
        setTimeout(() => {}, 2000);
        retorno.then((response) => {
            console.log('Resposta de obterDadosExtracao()');
            console.log(response);
            extratorEstatisticas._dadosExtracao = response.dadosExtracaoEstatisticas;
        }, (error) => {
            console.log('Erro ao consultar status da extracao: ' + error);
        });
    }

    salvarDadosExtracao() {
        let message = browser.runtime.sendMessage({
            'message': 'setDadosExtracaoEstatistica', 
            'data': this._dadosExtracao
        });
        setTimeout(() => {}, 2000);
        message.then((response) => {
            console.log('Resposta de salvarDadosExtracao()');
            console.log(response);
            extratorEstatisticas._dadosExtracao = response.dadosExtracaoEstatisticas;
        },
        (error) => {
            console.log('Erro ao salvar dados da extracao das estatisticas');
            console.log(error);
        });
    }
    
    montarFormularioExtracao() {
        let divBarraComandos = document.querySelector('div#divInfraBarraComandosSuperior');
        divBarraComandos.appendChild(this.montarCampoDataInicio());
        divBarraComandos.appendChild(this.montarCampoDataFim());
        divBarraComandos.appendChild(this.montarBotaoExtrair());
    }
    
    montarCampoDataInicio() {
        let dateInicio = document.createElement('input');
        dateInicio.setAttribute('type', 'date');
        dateInicio.setAttribute('id', 'dateInicio');
        
        let labelInicio = document.createElement('label');
        labelInicio.setAttribute('for', 'dateInicio');
        labelInicio.innerText = 'Início: ';
    
        let divDataInicio = document.createElement('div');
        divDataInicio.appendChild(labelInicio);
        divDataInicio.appendChild(dateInicio);

        return divDataInicio;
    }

    montarCampoDataFim() {
        let dateFim = document.createElement('input');
        dateFim.setAttribute('type', 'date');
        dateFim.setAttribute('id', 'dateFim');
        
        let labelFim = document.createElement('label');
        labelFim.setAttribute('for', 'dateFim');
        labelFim.innerText = 'Fim: ';
    
        let divDataFim = document.createElement('div');
        divDataFim.appendChild(labelFim);
        divDataFim.appendChild(dateFim);

        return divDataFim;
    }

    montarBotaoExtrair() {
        let botaoExtrair = document.createElement('button');
        botaoExtrair.setAttribute('id', 'botaoExtrair');
        botaoExtrair.innerText = 'Extrair';
        botaoExtrair.addEventListener('click', this.botaoExtrairOnClickListener);

        let divBotoes = document.createElement('div');
        divBotoes.setAttribute('id', 'divBotoes');
        divBotoes.appendChild(botaoExtrair);
        
        return divBotoes;
    }

    botaoExtrairOnClickListener(event) {
        event.preventDefault();
        extratorEstatisticas._dadosExtracao.extraindo = true;
        extratorEstatisticas.salvarDadosExtracao();
        document.location.reload();
    }

    botaoCancelarOnClickListener(event) {
        event.preventDefault();
        extratorEstatisticas._dadosExtracao.extraindo = false;
        extratorEstatisticas.salvarDadosExtracao();
        document.location.reload();
    }

}

var extratorEstatisticas = new ExtratorEstatisticas();
extratorEstatisticas.execute();