class PainelProcessos {

    constructor() {
        this._processos = [];
    }

    
    enviarMensagemAoBackground(mensagem, callback) {
        console.log('sendMessage:');
        console.log(mensagem);
        var retorno = browser.runtime.sendMessage(mensagem);
        retorno.then((response) => {
            console.log('sendMessage <- response:');
            console.log(response);
            callback(response);
        }, this.handleError);
    }

    handleError(error) {
        console.log('Erro ao enviar mensagem ao background: ' + error);
    }
    
    consultarSituacaoExtracao() {
        return this.enviarMensagemAoBackground({
            'message': 'extraindo'
        }, (response) => {
            console.log('Resposta de extraindo:');
            console.log(response);
            extraindoControleProcessos = response.data;
            return extraindoControleProcessos;
        });
    }

    iniciarExtracao() {
        this.enviarMensagemAoBackground({
            'message': 'iniciarExtracao'
        }, (response) => {
            console.log('iniciarExtracao <- response: ' + response.data);
            extraindoControleProcessos = response.data;
            this.lerProcessos(window);
        });
    }

    lerProcessos(win) {
        console.log('extraindoControleProcessos: ' + extraindoControleProcessos);
        if(extraindoControleProcessos) {
            let doc = win.document;
            let trs = doc.querySelectorAll('table#tblProcessosDetalhado tbody tr');
            trs.forEach(function(tr, indexTR) {
                if(indexTR > 0) {
                    console.log('Lendo processo ' + indexTR);
                    var usuario = new Usuario('Não atribuído', '00000000000');
                    var processo = new Processo('');
                    tr.querySelectorAll('td').forEach(function(td, iTD) {
                        // this nesse escopo é a variavel processo (ver no final da funcao aninhada)
    
                        //coluna anotação
                        if(iTD === 1) {
                            td.querySelectorAll('a').forEach(function(aTooltip, iA) {
                                // consultar href's específicos para cada caso (marcador, anotação, aviso, etc...)
                                // console.log(aTooltip.href);
                                if(aTooltip.href.includes('controlador.php?acao=anotacao_registrar')) {
                                    //console.log('Tem anotação');
                                    let onmouseover = aTooltip.getAttribute('onmouseover');
                                    // console.log(onmouseover);
                                    let anotacao = onmouseover.substring(onmouseover.indexOf("'")+1, onmouseover.indexOf("','"));
                                    // console.log('Anotação: ' + anotacao);
                                    processo.setAnotacao(anotacao);
                                    let anotacaoCPF = onmouseover.substring(onmouseover.indexOf("','")+3, onmouseover.indexOf("')"));
                                    // console.log('CPF: ' + anotacaoCPF);
                                    processo.setAnotacaoCPF(anotacaoCPF);
                                }
                                if(aTooltip.href.includes('controlador.php?acao=andamento_marcador_gerenciar')) {
                                    console.log('Tem marcador');
                                    let onmouseover = aTooltip.getAttribute('onmouseover');
                                    // console.log(onmouseover);
                                    let marcadorTexto = onmouseover.substring(onmouseover.indexOf("'")+1, onmouseover.indexOf("','"));
                                    // console.log('marcadorTexto: ' + marcadorTexto);
                                    processo.setMarcadorTexto(marcadorTexto);
                                    let marcador = onmouseover.substring(onmouseover.indexOf("','")+3, onmouseover.indexOf("')"));
                                    // console.log('marcador: ' + marcador);
                                    processo.setMarcador(marcador);
                                }
                                if(aTooltip.href.includes('javascript:void(0);')) {
                                    let imgRP = aTooltip.querySelector("img[src*='retorno_programado']");
                                    if(imgRP) {
                                        console.log('Tem retorno programado');
                                        let onmouseover = aTooltip.getAttribute('onmouseover');
                                        // console.log(onmouseover);
                                        let textoRetornoProgramado = onmouseover.substring(onmouseover.indexOf("'")+1, onmouseover.indexOf("','"));
                                        let arrRetornoProgramado = textoRetornoProgramado.split(" ");
                                        // console.log('areaRetornoProgramado: ' + arrRetornoProgramado[0]);
                                        processo.setAreaRetornoProgramado(arrRetornoProgramado[0]);
                                        // console.log('dataRetornoProgramado: ' + arrRetornoProgramado[1]);
                                        processo.setDataRetornoProgramado(arrRetornoProgramado[1]);
                                    }
                                }
                                
                            }, processo);
                        }
                
                        //coluna processo
                        if(iTD === 2) {
                            let aProcesso = td.querySelector('a');
                            let codProcesso = aProcesso.textContent;
                            // processo = new Processo(codProcesso, aProcesso);
                            this.setAElement(aProcesso);
                            this.setCodProcesso(codProcesso);
                        }
                        
                        // coluna usuário atribuído
                        if(iTD === 3) {
                            let aUsuarioAtribuido = td.querySelector('a');
                            if(aUsuarioAtribuido) {
                                let cpf = aUsuarioAtribuido.textContent;
                                let nome = aUsuarioAtribuido.getAttribute('title').substring(prefixoAtribuicao.length);
                                usuario = new Usuario(nome, cpf);
                            }
                        }
    
                        // tipo
                        if(iTD === 4) {
                            processo.setTipo(td.textContent);
                        }
        
                        processo.setUsuario(usuario);
                        
                    }, processo);
                    console.log(processo);
                    painelProcessos.registrarProcesso(processo, indexTR == trs.length - 1);
                }
            });
        }
    }

    registrarProcesso(processo, ultimoProcessoDaPagina) {
        // console.log(ultimoProcessoDaPagina);
        this.enviarMensagemAoBackground({
            'message': 'adicionarProcesso',
            'processo': {
                '_codProcesso': processo._codProcesso,
                '_usuario': processo._usuario,
                '_tipo': processo._tipo,
                '_anotacao': processo._anotacao,
                '_anotacaoCPF': processo._anotacaoCPF,
                '_marcador' : processo._marcador,
                '_marcadorTexto' : processo._marcadorTexto,
                '_areaRetornoProgramado' : processo._areaRetornoProgramado,
                '_dataRetornoProgramado' : processo._dataRetornoProgramado
            }
        }, (response) => {
            if(ultimoProcessoDaPagina) {
                this.finalizarLeituraDaPagina();
            }
        });
    }
    
    finalizarLeituraDaPagina() {
        console.log('Finalizando a leitura da página...');
        let aProximaPagina = document.querySelector('a#lnkInfraProximaPaginaSuperior');
        if(!aProximaPagina) {
            console.log('Última página lida.');
            this.gerarConteudo();
        } else {
            console.log('Vou chamar a próxima página.');
            aProximaPagina.click();
            // this.gerarConteudo(); //trocar pela linha de cima
        }
    }

    gerarConteudo() {
        this.conteudoDownload = 'Erro ao gerar arquivo';
        this.enviarMensagemAoBackground({
            'message': 'downloadProcessos'
        }, (response) => {
            // console.log(response);
            this.conteudoDownload = response.data;
            this.inserirLinkDownload();
        });
    }

    inserirLinkDownload() {
        var element = document.createElement('a');
        element.download = 'processos.csv';
        element.innerHTML = 'Baixar Processos';
        element.href = window.URL.createObjectURL(new Blob([this.conteudoDownload], {
            type: 'text/csv',
            charset: 'iso-8859-1'
        }));
        document.querySelector('div#divComandos').appendChild(element);
    }

    inserirBotaoIniciarExtracao() {
        if(!extraindoControleProcessos) {
            var botao = document.createElement('button');
            botao.innerHTML = 'Iniciar Extração';
            botao.addEventListener('click', (event) => {
                event.preventDefault();
                console.log(event);
                this.iniciarExtracao();
            });
            document.querySelector('div#divComandos').appendChild(botao);
        } else {
            var divStatusExtracao = document.createElement('div');
            divStatusExtracao.innerHTML = 'Aguarda... Lendo processos desta página...';
            document.querySelector('div#divComandos').appendChild(divStatusExtracao);
            this.lerProcessos(window);
        }
    }

    execute() {
        this.consultarSituacaoExtracao();
        this.inserirBotaoIniciarExtracao();
    }
}

var prefixoAtribuicao = 'Atribuído para ';
var painelProcessos = new PainelProcessos();
var extraindoControleProcessos = false;
painelProcessos.execute();
